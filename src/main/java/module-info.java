module com.example.batoipop {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;


    opens com.example.batoipop to javafx.fxml;
    exports com.example.batoipop;
    exports com.example.batoipop.Conexion;
    opens com.example.batoipop.Conexion to javafx.fxml;
    exports com.example.batoipop.DAO;
    opens com.example.batoipop.DAO to javafx.fxml;
    exports com.example.batoipop.Modelo;
    opens com.example.batoipop.Modelo to javafx.fxml;
}