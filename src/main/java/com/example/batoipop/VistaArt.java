package com.example.batoipop;

import com.example.batoipop.Conexion.ConexionBD;
import com.example.batoipop.Modelo.Articulo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.*;

public class VistaArt {
    private final File logoWhite = new File("src/main/resources/com/example/batoipop/Images/bpopiconwhite.png");
    private static final String SQLSELECTART = "SELECT * FROM batoi_pop_articulos";
    private static final String SQLSELECTNAMECAT = "SELECT name FROM batoi_pop_categorias where id = ?";
    private static final String SQLSELECTNAMEUSR = "SELECT name FROM batoi_pop_usuarios where id = ?";

    @FXML
    private ImageView logoPop;
    @FXML
    private TableColumn<Articulo, String> tCategoria;
    @FXML
    private TableColumn<Articulo, Boolean> tDestacado;
    @FXML
    private TableColumn<Articulo, Integer> tId;
    @FXML
    private TableColumn<Articulo, String> tNombre;
    @FXML
    private TableColumn<Articulo, Double> tPrecio;
    @FXML
    private TableColumn<Articulo, Integer> tUsuario;
    @FXML
    private TableView<Articulo> table;
    @FXML
    private VBox top;
    @FXML
    private Label name;

    private final ObservableList<Articulo> myData = FXCollections.observableArrayList();


    @FXML
    public void initialize() throws SQLException {
        logoPop.setImage(new Image(logoWhite.toURI().toString()));

        tId.setCellValueFactory(new PropertyValueFactory<Articulo, Integer>("id"));
        tNombre.setCellValueFactory(new PropertyValueFactory<Articulo, String>("nombre"));
        tCategoria.setCellValueFactory(new PropertyValueFactory<Articulo, String>("categoria"));
        tDestacado.setCellValueFactory(new PropertyValueFactory<Articulo, Boolean>("destacado"));
        tPrecio.setCellValueFactory(new PropertyValueFactory<Articulo, Double>("precio"));
        tUsuario.setCellValueFactory(new PropertyValueFactory<Articulo, Integer>("usuario"));

        table.setItems(myData);

        Connection con = ConexionBD.getConexion();

        PreparedStatement pst = con.prepareStatement(SQLSELECTART);
        ResultSet rs = pst.executeQuery();

        while (rs.next()){
            String nameCat = "";

            PreparedStatement pstCat = con.prepareStatement(SQLSELECTNAMECAT);
            pstCat.setInt(1, rs.getInt("categoria"));
            ResultSet rsCat = pstCat.executeQuery();

            if (rsCat.next()){
                nameCat = rsCat.getString("name");
            }

            String nameUsr = "";

            PreparedStatement pstUsr = con.prepareStatement(SQLSELECTNAMEUSR);
            pstUsr.setInt(1, rs.getInt("usuarios"));
            ResultSet rsUsr = pstUsr.executeQuery();

            if (rsUsr.next()){
                nameUsr = rsUsr.getString("name");
            }

            Articulo a = new Articulo(rs.getInt("id"), rs.getString("name"), rs.getDouble("precio"), nameCat, nameUsr, rs.getBoolean("destacado"));

            myData.add(a);

            table.setRowFactory( tv -> {
                TableRow<Articulo> row = new TableRow<>();
                row.setOnMouseClicked(event -> {
                    if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                        Articulo rowData = row.getItem();
                        try {
                            dobleClick(rowData);
                        } catch (IOException | SQLException e) {
                            e.printStackTrace();
                        }
                    }
                });
                return row ;
            });
        }
    }

    private void dobleClick(Articulo rowData) throws IOException, SQLException {
        Stage stageActual = (Stage) top.getScene().getWindow();
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage newStage = new Stage();
        newStage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("articulos.fxml"));

        Parent root = loader.load();

        Articulos controller = loader.getController();

        controller.setId(rowData.getId());
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        newStage.setX(x);
        newStage.setY(y);

        newStage.setScene(scene);
        newStage.setTitle("BATOIPOP");

        newStage.show();

        stageActual.close();
    }

    @FXML
    void cerrarSesion(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));

        Parent root = loader.load();

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void denuncias(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);

        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("denuncias.fxml"));

        Parent root = loader.load();

        Denuncias controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void articulos(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("articulos.fxml"));

        Parent root = loader.load();

        Articulos controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void bd(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gestionBD.fxml"));

        Parent root = loader.load();

        GestionBD controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    public void setName (String name){
        this.name.setText(name);
    }

    private Stage getStage(ActionEvent event) {
        return (Stage) ((Node) event.getSource()).getScene().getWindow();
    }
}
