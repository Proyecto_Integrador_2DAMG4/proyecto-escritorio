package com.example.batoipop;

import com.example.batoipop.Conexion.ConexionBD;
import com.example.batoipop.Modelo.Articulo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class Articulos {
    private final File logoWhite = new File("src/main/resources/com/example/batoipop/Images/bpopiconwhite.png");
    private static final String SQLSELECTART = "SELECT * from batoi_pop_articulos where id = ?";
    private static final String SQLSELECTCAT = "SELECT * from batoi_pop_categorias";
    private static final String SQLUPDATECAT = "UPDATE batoi_pop_articulos SET categoria = ?, destacado = ? where id = ?";
    private static final String SQLDELETEART = "DELETE FROM batoi_pop_articulos WHERE id = ?";
    private static final String SQLDELETEMEN = "DELETE FROM batoi_pop_mensajes WHERE articulo = ?";
    private static final String SQLSELECTNAMEUSR = "SELECT * FROM batoi_pop_usuarios where id = ?";
    private static final String SQLSELECTNAMECAT = "SELECT * FROM batoi_pop_categorias where id = ?";
    private static final String SQLSELECTCATID = "SELECT * FROM batoi_pop_categorias where name = ?";

    List<String> lista = new ArrayList<>();
    int id = 0;
    Articulo articulo;

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    private TextArea desc;

    @FXML
    private TextField idArticulo;

    @FXML
    private CheckBox destacado;

    @FXML
    private ImageView logoPop;

    @FXML
    private Label nameArt;

    @FXML
    private Label priceArt;

    @FXML
    private Label name;

    @FXML
    private ImageView fotoArt;

    @FXML
    void buscar() throws SQLException {

        if (idArticulo.getText() == null || Objects.equals(idArticulo.getText(), "")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("ERROR");
            alert.setContentText("El ID del articulo no puede estar vacio");
        } else {
            Connection con = ConexionBD.getConexion();

            PreparedStatement pstSelectPswd = con.prepareStatement(SQLSELECTART);
            pstSelectPswd.setInt(1, Integer.parseInt(idArticulo.getText()));
            ResultSet rs = pstSelectPswd.executeQuery();

            if (rs.next()) {
                nameArt.setText(rs.getString("name"));
                priceArt.setText(String.valueOf(rs.getDouble("precio")) + "€");
                desc.setText(rs.getString("descripcion"));
                destacado.setSelected(rs.getBoolean("destacado"));
                //Image image = new Image("http://localhost:8069/web/image/?model=batoi_pop.articulos&field=imagen&session_id=274af6c01e4df6ed23748cbd3b1d353a734b27f0&id=" + idArticulo, true);
                Image image = new Image("http://137.74.226.44:8069/web/image/?model=batoi_pop.articulos&field=imagen&session_id=fecd4af087a926cdaec5af80612d78bb9acc4709&id=" + idArticulo.getText(), true);
                fotoArt.setImage(image);

                PreparedStatement pst = con.prepareStatement(SQLSELECTNAMECAT);
                pst.setInt(1, rs.getInt("categoria"));
                ResultSet rs2 = pst.executeQuery();

                String nameCat = "";

                if (rs2.next()) {
                    nameCat = rs2.getString("name");
                }

                for (int i = 0; i <= lista.size(); i++) {

                    if (Objects.equals(nameCat, lista.get(i))) {
                        comboBox.getSelectionModel().select(i);
                        break;
                    }
                }

                PreparedStatement pstUsr = con.prepareStatement(SQLSELECTNAMEUSR);
                pstUsr.setInt(1, rs.getInt("usuarios"));
                ResultSet rsUsr = pstUsr.executeQuery();

                String nameUsr = "";
                if (rsUsr.next()) {
                    nameUsr = rsUsr.getString("name");
                }

                articulo = new Articulo(rs.getInt("id"), rs.getString("name"), rs.getDouble("precio"), nameCat, nameUsr, rs.getBoolean("destacado"));

            } else {
                mostrarAlert("El articulo con ID " + idArticulo.getText() + " no existe.");
            }
        }
    }

    @FXML
    void actualizar(ActionEvent event) throws SQLException {
        if ((idArticulo.getText() == null || Objects.equals(idArticulo.getText(), "")) && (articulo == null)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("ERROR AL ACTUALIZAR");
            alert.setContentText("No has seleccionado ningun articulo.");
            alert.show();
        } else {
            Connection con = ConexionBD.getConexion();
            PreparedStatement pstSelectPswd = con.prepareStatement(SQLUPDATECAT);

            pstSelectPswd.setInt(1, buscarNombre(comboBox.getSelectionModel().getSelectedItem()));
            pstSelectPswd.setBoolean(2, destacado.isSelected());
            pstSelectPswd.setString(3, idArticulo.getText());

            String destacadoS;
            if (destacado.isSelected()) {
                destacadoS = "esta";
            } else {
                destacadoS = "no esta";
            }

            pstSelectPswd.setInt(3, articulo.getId());
            pstSelectPswd.executeUpdate();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("ACTUALIZACION DE LA CATEGORIA");
            alert.setContentText("La categoria del producto " + articulo.getNombre() + " se ha actualizado con exito a " + lista.get(comboBox.getSelectionModel().getSelectedIndex()).toLowerCase(Locale.ROOT) + " y " + destacadoS + " destacado");
            alert.show();
        }
    }

    private int buscarNombre(String selectedItem) throws SQLException {
        Connection con = ConexionBD.getConexion();
        PreparedStatement pstSelectPswd = con.prepareStatement(SQLSELECTCATID);

        pstSelectPswd.setString(1, selectedItem);
        ResultSet rs = pstSelectPswd.executeQuery();

        int id = 0;

        if (rs.next()) {
            id = rs.getInt("id");
            System.out.println(id);
        }

        return id;
    }

    @FXML
    void eliminar(ActionEvent event) throws SQLException {
        Connection con = ConexionBD.getConexion();

        PreparedStatement pstDelete = con.prepareStatement(SQLDELETEMEN);
        pstDelete.setInt(1, articulo.getId());
        pstDelete.execute();

        pstDelete = con.prepareStatement(SQLDELETEART);
        pstDelete.setInt(1, articulo.getId());
        pstDelete.execute();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("ARTICULO ELIMINADO");
        alert.setContentText("EL articulo " + articulo.getNombre() + " se ha eliminado con exito");
        alert.show();

        nameArt.setText("");
        priceArt.setText("");
        desc.setText("");
        idArticulo.setText("");
        comboBox.getSelectionModel().clearSelection();
        destacado.setSelected(false);
    }

    private void comboBoxItems() throws SQLException {
        Connection con = ConexionBD.getConexion();

        PreparedStatement pstSelectPswd = con.prepareStatement(SQLSELECTCAT);
        ResultSet rs = pstSelectPswd.executeQuery();


        while (rs.next()) {
            lista.add(rs.getString("name"));
        }

        ObservableList<String> observableList = FXCollections.observableList(lista);

        comboBox.setItems(observableList);
    }

    private void mostrarAlert(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("ERROR AL BUSCAR ARTICULO");
        alert.setContentText(mensaje);
        alert.show();
    }

    @FXML
    public void initialize() throws SQLException {
        logoPop.setImage(new Image(logoWhite.toURI().toString()));

        comboBoxItems();
    }

    public void setId(int id) throws SQLException {
        this.id = id;
        idArticulo.setText(String.valueOf(id));
        buscar();
    }

    @FXML
    void cerrarSesion(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));

        Parent root = loader.load();

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();
        stageActual.close();
    }

    @FXML
    void bd(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gestionBD.fxml"));

        Parent root = loader.load();

        GestionBD controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void destacados(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("vistaArt.fxml"));

        Parent root = loader.load();

        VistaArt controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void denuncias(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("denuncias.fxml"));

        Parent root = loader.load();

        Denuncias controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    public void setName(String name) {
        this.name.setText(name);
    }

    private Stage getStage(ActionEvent event) {
        return (Stage) ((Node) event.getSource()).getScene().getWindow();
    }


}
