package com.example.batoipop;

import com.example.batoipop.Conexion.ConexionBD;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.Objects;

public class LoginPop {
    private final File logoWhite = new File("src/main/resources/com/example/batoipop/Images/bpopiconwhite.png");
    private static final String SQLSELECTEMPLEADOS = "select * from batoi_pop_empleados where email = ?";

    @FXML
    private ImageView logoPop;

    @FXML
    private TextField nombre;

    @FXML
    private PasswordField contrasena;

    private String nombreEmpleados;

    private boolean comprobarDatos() throws SQLException {
        boolean nextStage = false;
        boolean alert = false;

        Connection con = ConexionBD.getConexion();

        PreparedStatement pstSelectPswd = con.prepareStatement(SQLSELECTEMPLEADOS);
        pstSelectPswd.setString(1, nombre.getText());
        ResultSet rs = pstSelectPswd.executeQuery();

        alert = comprobarVacio();

        if (rs.next()) {
            if (contrasena.getText().equals(String.valueOf(rs.getString("password")))){
                nombreEmpleados = rs.getString("name");
                nextStage = true;
            } else {
                mostrarAlert("Contraseña incorrecta");
            }
        } else if (!alert){
            mostrarAlert("El email introducido no existe");
        }

        return nextStage;
    }

    private boolean comprobarVacio() {
        String mensaje;
        boolean alert = false;

        if ((nombre.getText() == null || Objects.equals(nombre.getText(), "")) && (contrasena.getText() == null || Objects.equals(contrasena.getText(), ""))){
            mensaje = "Introduce el email y la contraseña";
            mostrarAlert(mensaje);
            alert = true;

        } else if (nombre.getText() == null || Objects.equals(nombre.getText(), "")){
            mensaje = "Introduce el email";
            mostrarAlert(mensaje);
            alert = true;

        } else if (contrasena.getText() == null || Objects.equals(contrasena.getText(), "")){
            mensaje = "Introduce la contraseña";
            mostrarAlert(mensaje);
            alert = true;
        }

        return alert;
    }

    private void mostrarAlert(String mensaje) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("ERROR AL INICIAR SESIÓN");
        alert.setContentText(mensaje);
        alert.show();
    }

    @FXML
    void login(ActionEvent event) throws IOException, SQLException {

        if (comprobarDatos()){
            Stage stageActual = getStage(event);
            double height = stageActual.getHeight();
            double width = stageActual.getWidth();
            double x = stageActual.getX();
            double y = stageActual.getY();

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("menuTrabajadores.fxml"));

            Parent root = loader.load();

            MenuTrabajadores controller = loader.getController();
            controller.setName(nombreEmpleados);

            Scene scene = new Scene(root, width, height);

            stage.setX(x);
            stage.setY(y);

            stage.setScene(scene);
            stage.setTitle("BATOIPOP");

            stage.show();

            stageActual.close();
        }
    }

    private Stage getStage(ActionEvent event) {
        return (Stage) ((Node) event.getSource()).getScene().getWindow();
    }

    @FXML
    public void initialize(){
        logoPop.setImage(new Image(logoWhite.toURI().toString()));
    }

}