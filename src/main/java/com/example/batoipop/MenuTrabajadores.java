package com.example.batoipop;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class MenuTrabajadores {

    private final File logoWhite = new File("src/main/resources/com/example/batoipop/Images/bpopiconwhite.png");
    private final File logoBlue = new File("src/main/resources/com/example/batoipop/Images/bpopIconblue.png");

    @FXML
    private ImageView logoPop;
    @FXML
    private ImageView logoPop2;
    @FXML
    private Label name;

    @FXML
    public void initialize(){
        logoPop.setImage(new Image(logoWhite.toURI().toString()));
        logoPop2.setImage(new Image(logoBlue.toURI().toString()));
    }

    @FXML
    void cerrarSesion(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));

        Parent root = loader.load();

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void denuncias(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("denuncias.fxml"));

        Parent root = loader.load();

        Denuncias controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void destacados(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("vistaArt.fxml"));

        Parent root = loader.load();

        VistaArt controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void articulos(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("articulos.fxml"));

        Parent root = loader.load();

        Articulos controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void bd(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gestionBD.fxml"));

        Parent root = loader.load();

        GestionBD controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    private Stage getStage(ActionEvent event) {
        return (Stage) ((Node) event.getSource()).getScene().getWindow();
    }

    public void setName (String name){
        this.name.setText(name);
    }
}
