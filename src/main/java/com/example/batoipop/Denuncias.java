package com.example.batoipop;

import com.example.batoipop.Conexion.ConexionBD;
import com.example.batoipop.Modelo.Denuncia;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Denuncias {

    private final File logoWhite = new File("src/main/resources/com/example/batoipop/Images/bpopiconwhite.png");
    private static final String SQLSELECTDEN = "SELECT * FROM batoi_pop_denuncia_articulo";
    private static final String SQLUPDATE = "update batoi_pop_denuncia_articulo set resuelta = true where id = ?";

    @FXML
    private ImageView logoPop;
    @FXML
    private TableColumn<Denuncia, String> tDescripcion;
    @FXML
    private TableColumn<Denuncia, String> tMotivo;
    @FXML
    private TableColumn<Denuncia, Integer> tId;
    @FXML
    private TableView<Denuncia> table;
    @FXML
    private Label name;

    private final ObservableList<Denuncia> myData = FXCollections.observableArrayList();


    @FXML
    public void initialize() throws SQLException, IOException {
        logoPop.setImage(new Image(logoWhite.toURI().toString()));

        tId.setCellValueFactory(new PropertyValueFactory<Denuncia, Integer>("id"));
        tMotivo.setCellValueFactory(new PropertyValueFactory<Denuncia, String>("motivo"));
        tDescripcion.setCellValueFactory(new PropertyValueFactory<Denuncia, String>("descripcion"));

        table.setItems(myData);

        agregarDenuncias();
    }

    private void agregarDenuncias() throws SQLException {
        Connection con = ConexionBD.getConexion();

        PreparedStatement pst = con.prepareStatement(SQLSELECTDEN);
        ResultSet rs = pst.executeQuery();

        while (rs.next()){
            Denuncia den = new Denuncia(rs.getInt("id"), rs.getString("motivo"), rs.getString("descripcion"), rs.getBoolean("resuelta"), rs.getInt("prioridad"));
            myData.add(den);

            table.setRowFactory(tv -> new TableRow<Denuncia>() {
                @Override
                protected void updateItem(Denuncia item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null)
                        setStyle("");
                    else if (item.isResuelta())
                        setStyle("-fx-background-color: #1EFF00;");
                    else if (item.getPrioridad() == 1)
                        setStyle("-fx-background-color: #FFFA27;");
                    else if (item.getPrioridad() == 2)
                        setStyle("-fx-background-color: #FFAE1B;");
                    else if (item.getPrioridad() == 3)
                        setStyle("-fx-background-color: #FF1F1F;");
                    else
                        setStyle("");
                }
            });
        }
    }

    @FXML
    void resuelta() {
        Denuncia clickedRow = table.getSelectionModel().getSelectedItem();

        if (clickedRow == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("ERROR AL RESOLVER LA DENUNCIA");
            alert.setContentText("No se ha seleccionado ninguna denuncia");
            alert.show();
        } else {

            Connection con = null;
            try {
                con = ConexionBD.getConexion();

                PreparedStatement pstSelectPswd = con.prepareStatement(SQLUPDATE);
                pstSelectPswd.setInt(1, clickedRow.getId());
                pstSelectPswd.executeUpdate();

                clickedRow.setResuelta(true);

                myData.clear();
                agregarDenuncias();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    void cerrarSesion(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("hello-view.fxml"));

        Parent root = loader.load();

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();
        stageActual.close();
    }

    @FXML
    void bd(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("gestionBD.fxml"));

        Parent root = loader.load();

        GestionBD controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void destacados(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("vistaArt.fxml"));

        Parent root = loader.load();

        VistaArt controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    @FXML
    void articulos(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("articulos.fxml"));

        Parent root = loader.load();

        Articulos controller = loader.getController();
        controller.setName(name.getText());

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    public void setName (String name){
        this.name.setText(name);
    }

    private Stage getStage(ActionEvent event) {
        return (Stage) ((Node) event.getSource()).getScene().getWindow();
    }

}
