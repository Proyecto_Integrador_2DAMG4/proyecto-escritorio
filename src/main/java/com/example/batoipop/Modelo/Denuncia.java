package com.example.batoipop.Modelo;

import java.util.SplittableRandom;

public class Denuncia {
    private int id;
    private String motivo;
    private String descripcion;
    private boolean resuelta;
    private int prioridad;

    public Denuncia(int id, String motivo, String descripcion, boolean resuelta, int prioridad) {
        this.id = id;
        this.motivo = motivo;
        this.descripcion = descripcion;
        this.resuelta = resuelta;
        this.prioridad = prioridad;
    }

    public int getId() {
        return id;
    }

    public String getMotivo() {
        return motivo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public boolean isResuelta() {
        return resuelta;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setResuelta(boolean resuelta) {
        this.resuelta = resuelta;
    }
}
