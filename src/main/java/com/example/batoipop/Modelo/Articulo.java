package com.example.batoipop.Modelo;

public class Articulo {
    private int id;
    private String nombre;
    private double precio;
    private String categoria;
    private String usuario;
    private boolean destacado;

    public Articulo(int id, String nombre, double precio, String categoria, String usuario, boolean destacado) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.categoria = categoria;
        this.usuario = usuario;
        this.destacado = destacado;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getUsuario() {
        return usuario;
    }

    public boolean isDestacado() {
        return destacado;
    }
}
