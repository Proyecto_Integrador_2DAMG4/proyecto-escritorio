package com.example.batoipop;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class MenuPrincipal {

    private final File logoWhite = new File("src/main/resources/com/example/batoipop/Images/bpopiconwhite.png");
    private final File logoBlue = new File("src/main/resources/com/example/batoipop/Images/bpopIconblue.png");

    @FXML
    private ImageView logoPop;
    @FXML
    private ImageView logoPop2;

    @FXML
    void login(ActionEvent event) throws IOException {
        Stage stageActual = getStage(event);
        double height = stageActual.getHeight();
        double width = stageActual.getWidth();
        double x = stageActual.getX();
        double y = stageActual.getY();

        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("loginPop.fxml"));

        Parent root = loader.load();

        Scene scene = new Scene(root, width, height);

        stage.setX(x);
        stage.setY(y);

        stage.setScene(scene);
        stage.setTitle("BATOIPOP");

        stage.show();

        stageActual.close();
    }

    private Stage getStage(ActionEvent event) {
        return (Stage) ((Node) event.getSource()).getScene().getWindow();
    }

    @FXML
    public void initialize(){
        logoPop.setImage(new Image(logoWhite.toURI().toString()));
        logoPop2.setImage(new Image(logoBlue.toURI().toString()));
    }
}